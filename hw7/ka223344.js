var tabselect = "";
var ipreqback = "";
var plat = "";
var plng = "";
var pdis = "";
var nextPage = "";
var nextcount = "";
var nextplacecount = "";
var nextplacePage = "";
var clicked = "";
var prevPage = "";
var prevcount = "";
var next = "next";
var prev = "prev";
var yes = "yes";
var detailsElement = "";
var favcounter = 0;
var fcounter = 0;
var delIDObj = "";
var userID = "";
var clickedRow = "";
var clickedFavtype = "";

var app = angular.module("myApp1", ["ngAnimate"]);
app.controller("MyCtrl", function($scope) {
    $scope.myValue = true;

});

function clearAll() {

    $('#inputSearchText').val("");
    $('#mainTableDiv').css('display', 'none');
    $('#test').css('display', 'none');
    $('#inputSearchText').tooltip('destroy')
    $('#userTabs a[href="user"]').tab('show');
    var appElement = document.querySelector('[ng-app=myApp1]');
    var $scope = angular.element(appElement).scope();
    $scope.$apply(function() {
    $scope.myValue = true;
    });

}

var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};

function success(pos) {
    var crd = pos.coords;
    console.log('Your current position is:');
    console.log(`Latitude : ${crd.latitude}`);
    console.log(`Longitude: ${crd.longitude}`);
    console.log(`More or less ${crd.accuracy} meters.`);
    plat = crd.latitude;
    plng = crd.longitude;
    pdis = crd.accuracy;
};

function error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
};

navigator.geolocation.getCurrentPosition(success, error, options);

$(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function(e) {
    var target = $(e.target).attr("href") // activated tab
    
    var appElement = document.querySelector('[ng-app=myApp1]');
    var $scope = angular.element(appElement).scope();
    $scope.$apply(function() {
    $scope.myValue = true;
    });
    

    var ipreq = $('#inputSearchText').val();
    tabselect = target;
    ipreqback = ipreq;

    if (tabselect != "place") {
        if (ipreq != "" && clicked == "y") {
            $('#mainTableDiv').html('')
            $('#test').html('')
            testonChange(ipreq, tabselect);
        } else {

        }
    } else {
        if (ipreq != "" && clicked == "y") {

            $('#mainTableDiv').html('')
            $('#test').html('')
            testonChange(ipreq, tabselect);
        } else {

        }

    }

})

function closeTip() {
    $('#inputSearchText').tooltip('destroy')
}


function test() {
    clicked = "y";
    var activeTab = null;
    tabselect = $('.nav-tabs .active > a').attr('href')
    var ipreq = $('#inputSearchText').val();
    ipreqback = ipreq;
    if (ipreq != "") {
        $('#mainTableDiv').css('display', 'block');
    } else {

        $('#inputSearchText').tooltip('show')
    }


    if (tabselect != "place") {
        if (ipreq != "") {
            $('#mainTableDiv').html('')
            $('#test').html('')
            testonChange(ipreq, tabselect);
        } else {

        }
    } else {
        if (ipreq != "") {

            $('#mainTableDiv').html('')
            $('#test').html('')
            testonChange(ipreq, tabselect);
        } else {

        }

    }

}




function testonChange(ipreq, tabselect) {
    nextcount = "";
    // alert(target);

    if ($('#mainTableDiv').html('')) {
        $('#mainTableDiv').css('display', 'block')
        $('#mainTableDiv').html('<div id="maintablepbar" class="container progress">' +
            '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%" style = "position: absolute;top:0;bottom: 0;left: 0;right: 0;margin: auto;"></div></div>');
    }



    if (tabselect == "fav") {
        $('#mainTableDiv').html('');

        var counter1 = 0;

        console.log(localStorage.length)


        var $table = $('<table class="table table-hover"><thead><tr><th>#</th><th>Profile Photo</th><th>Name</th><th>Type</th><th>Favorite</th><th>Details</th></tr></thead><tbody>');

        var trashbtn = '<button type="button" ng-click="myValue=false;" class="btn btn-default btn-lg" onClick="testDetailsCall();return false;"><span class="glyphicon glyphicon-trash"></span></button>';

        var detailsbtn = '<button type="button" ng-click="myValue=false;" class="btn btn-default btn-lg" onClick="testDetailsCall();return false;"><span class="glyphicon glyphicon-chevron-right"></span></button>';


        if (localStorage.length > 0) {
            var favArray = [];

            for (var arrVal = 0, len = localStorage.length; arrVal < len; ++arrVal) {
                favArray[arrVal] = localStorage.key(arrVal);
            }

        }
        
        if(localStorage.length>0)
            
{
    var $rowCount = 1;
        for (var i = 0, len = localStorage.length; i < len; i++) {


            var aget = localStorage.getItem(localStorage.key(i));
            var pget = JSON.parse(aget);
            if (pget != null) {

                var $row = $('<tr>').addClass('favbar');
                var $fdcount = $('<td>').text($rowCount);
                var $fdImg = $('<td>').append(pget.ppic);
                var $fdName = $('<td>').text(pget.pname);
                var $fdType = $('<td>').text(pget.ptype);
                var $fdID = $('<td>').text(pget.pid);
var $fdTrash = $('<td>').append("<button type='button' class='btn btn-default btn-lg' onClick='testDeleteFav(this," + pget.pid + ");return false;'><span class='glyphicon glyphicon-trash'></span></button>");
var $fdDetails = $('<td>').append("<button type='button' ng-click='myValue=false;' class='btn btn-default btn-lg' onClick='testDetailsCall(this," + pget.pid + ");return false;'><span class='glyphicon glyphicon-chevron-right''></span></button>");

                $fdcount.appendTo($row);
                $fdImg.appendTo($row);
                $fdName.appendTo($row);
                $fdType.appendTo($row);
                $fdTrash.appendTo($row);
                $fdDetails.appendTo($row);

                $row.appendTo($table);
                $rowCount++;
            }



        }
        
    }
        
        else
            {
                var $emptyTable = $('<tr><td colspan="6"><div style="text-align:center" class="panel panel-warning">' +
                    '<div class="panel-heading">No data found' +
                    '</div></td></tr>' +
                    '</tbody>');
                $emptyTable.appendTo($table);
            }
        $('#maintablepbar').css('display', 'none');
        $('#mainTableDiv').html($table);
        $('#mainTableDiv').css('display', 'block');
        $('#test').css('display', 'none');


    }




    $.ajax({
        type: 'GET',
        url: 'http://fb-test-app-new.appspot.com/main.php?',
        data: {
            value: ipreq,
            type: tabselect,
            latitude: plat,
            longitude: plng,
            distance: pdis
        },
        dataType: 'json',
        success: function(data) {

            console.log(data);

            if (typeof data.paging !== "undefined") {


                if (typeof data.paging.next !== "undefined") {



                    nextButton = "<button type='button' class='btn btn-default' onclick='gotoNext(" + next + ");return false;'>Next</button>"

                    nextcount = "y";
                    nextPage = data.paging.next;


                } else {
                    nextcount = "n";
                    console.log("no paging");
                }


            } else {
                nextcount = "n";
                console.log("no paging");

            }

            var table = $('<table class="table table-hover"><thead><tr><th>#</th><th>Profile Photo</th><th>Name</th>     <th>Favorite</th><th>Details</th></tr></thead><tbody>');
            ht = "";
            var testHidden = $('<div id="one" class="well animate-hide" ng-hide="myValue"> <button class="btn btn-primary" ng-click="myValue=true">Go Back</button>');


            var detailsbtn = '<button type="button" ng-click="myValue=false;" class="btn btn-default btn-lg" onClick="testDetailsCall();return false;"><span class="glyphicon glyphicon-chevron-right"></span></button>';

            var newbtn = '<button class="btn btn-default" ng-click="myValue=false;">Details</button>';



            if (localStorage.length > 0) {
                var favArray = [];

                for (var arrVal = 0, len = localStorage.length; arrVal < len; ++arrVal) {
                    favArray[arrVal] = localStorage.key(arrVal);
                }

            }




            for (var i = 0, count = 1; i < data.data.length; i++, count++) {
                var imgSrc1 = data.data[i].picture.data.url

                var row = $('<tr>').addClass('bar')

                for (var j = 0; j < 1; j++) {



                    var img = $('<img id="newImagedynamic">'); //Equivalent: $(document.createElement('img'))
                    img.attr('src', imgSrc1);
                    img.attr('class', 'img-circle');
                    img.attr('width', '30px');
                    img.attr('height', '30px');
                    img.attr('alt', 'Profile photo');

                    var tdcount = $('<td>').text(count);
                    var tdImg = $('<td>').append(img);
                    var tdName = $('<td>').text(data.data[i].name);

                    if (localStorage.length > 0) {
                        if (favArray.includes(data.data[i].id)) {

                            {
                                var tdDummyID = $('<td>').append('<button id="favbtn" onclick="getUserID(this,' + data.data[i].id + ');return false;" type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-star bis"></span></button>');
                            }

                        } else {
                            var tdDummyID = $('<td>').append('<button id="favbtn" onclick="getUserID(this,' + data.data[i].id + ');return false;" type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-star-empty"></span></button>');
                        }

                    } else {
                        var tdDummyID = $('<td>').append('<button id="favbtn" onclick="getUserID(this,' + data.data[i].id + ');return false;" type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-star-empty"></span></button>');
                    }




                    var tdDetails = $('<td>').append("<button type='button' ng-click='myValue=false;' class='btn btn-default btn-lg' onClick='testDetailsCall(this," + data.data[i].id + ");return false;'><span class='glyphicon glyphicon-chevron-right''></span></button>");

                }
                tdcount.appendTo(row);
                tdImg.appendTo(row);
                tdName.appendTo(row);
                tdDummyID.appendTo(row);
                tdDetails.appendTo(row);

                row.appendTo(table);

            }



            $('#maintablepbar').css('display', 'none');
            $('#mainTableDiv').css('height', '')
            $('#mainTableDiv').html(table);
            $('#mainTableDiv').css('display', 'block');




            if (tabselect == "fav") {
                $('#test').css('display', 'none');
            } else if (nextcount != "n") {

                {
                    $('#mainTableDiv').css('height', 'auto')
                    $('#test').css('display', 'block');
                    $('#test').html(nextButton);

                }
            } else {
                $('#test').css('display', 'none');
            }

        }


    });

}

function testDeleteFav(Object,trashID) {
    console.log(Object);
    Object.parentNode.parentNode.remove();
    if (localStorage.length != 0) {
        localStorage.removeItem(trashID.toString());
    }
    testonChange(ipreqback, tabselect);
}



function getUserID(Object1, idObject) {

    delIDObj = idObject;
    console.log(Object1)
    console.log(idObject)

   // var favName = this.document.activeElement.parentElement.previousElementSibling.innerText;
    //var favPic = this.document.activeElement.parentElement.previousElementSibling.previousElementSibling.innerHTML;
    //var favCount = this.document.activeElement.parentElement.previousElementSibling.previousElementSibling.previousElementSibling.innerText;
    
    var favName = Object1.parentNode.previousSibling.innerHTML;
    var favPic = Object1.parentNode.previousSibling.previousSibling.innerHTML;
    var favCount = Object1.parentNode.previousSibling.previousSibling.previousSibling.innerHTML;
    
    console.log(favName);
    console.log(favPic);
    console.log(favCount);
    
    

    var a = Object1.firstChild;
    console.log(a)
    var b = a.className;
    console.log(b);


    var i = localStorage.length + 1;

    if (b == "glyphicon glyphicon-star bis") {
        Object1.firstChild.className = "glyphicon glyphicon-star-empty"
        //a.removeClass('glyphicon glyphicon-star bis');
        //a.addClass('glyphicon glyphicon-star-empty');
        b = Object1.firstChild.className;
        console.log(b)


        for (var i = 0, len = localStorage.length; i < len; ++i) {
            var aget2 = localStorage.getItem(localStorage.key(i));
            var pget2 = JSON.parse(aget2);
            if (pget2 != null) {
                if (pget2.ptype == tabselect && pget2.pid == delIDObj) {
                    localStorage.removeItem(delIDObj.toString());
                }
            }


        }




    } else {

       // a.removeClass('glyphicon glyphicon-star-empty');
        //a.addClass('glyphicon glyphicon-star bis');
        Object1.firstChild.className ="glyphicon glyphicon-star bis";
        var ab = {
            'ppic': favPic,
            'pname': favName,
            'pid': idObject,
            'pcount': favCount,
            'ptype': tabselect
        };
        localStorage.setItem(delIDObj.toString(), JSON.stringify(ab));
        b = Object1.firstChild.className;
        console.log(b)
        console.log(favPic)
    }

}




$('#favbtn').click(function() {
    console.log(this);



});




function testBackCall() {
 //$('#mainTableDiv').css('display','block');
    if (tabselect == "fav") {
        testonChange(ipreqback, tabselect);
    }

    var appElement = document.querySelector('[ng-app=myApp1]');
    var $scope = angular.element(appElement).scope();
    $scope.$apply(function() {
        $scope.myValue = true;
    });

    $('#accordion').html('')
    $('#postsRow').html('');
}

function testAsync(object) {
    alert("hello")
}


function favbtnDetailClick(newusrObj) {


    console.log(newusrObj);

    var upic = '<img id="newImagedynamic" src=' + newusrObj.picURL + ' class="img-circle" alt="Profile photo" height="30px" width="30px">';

    var detailFav = $('#favbtnDetail').children("span").attr('class');
    console.log(detailFav)
    if (detailFav == "glyphicon glyphicon-star bis") {
        $("#favbtnDetail").children('span').attr('class', 'glyphicon glyphicon-star-empty');

        clickedRow.className = "glyphicon glyphicon-star-empty"



        for (var i = 0, len = localStorage.length; i < len; ++i) {
            var aget2 = localStorage.getItem(localStorage.key(i));
            var pget2 = JSON.parse(aget2);
            if (pget2 != null) {
                if (pget2.pid == newusrObj.uid) {
                    localStorage.removeItem(newusrObj.uid.toString());
                }
            }


        }




    } else {
        $("#favbtnDetail").children('span').attr('class', 'glyphicon glyphicon-star bis');

        var ab = {
            'ppic': upic,
            'pname': newusrObj.uname,
            'pid': newusrObj.uid,
            'ptype': tabselect
        };
        localStorage.setItem(newusrObj.uid.toString(), JSON.stringify(ab));
        clickedRow.className = "glyphicon glyphicon-star bis"

    }


}

function fbPostClick(Object) {
    console.log(Object);
    FB.ui({
        method: 'share',
        href: 'https://developers.facebook.com/docs/',
        title: Object.uname,
        caption: 'FB SEARCH FROM USC CSCI 571',
        picture: Object.picURL,
    }, function(response) {
        if (typeof response !== "undefined") {
            alert("Posted Successfully")
        } else {
            alert("Not Posted")
        }

    });

}



function testDetailsCall(Object2,idObject) {
    
    console.log(Object2);
    console.log(idObject);
    $('body').scrollTop(0);
    
    
    var appElement = document.querySelector('[ng-app=myApp1]');
    var $scope = angular.element(appElement).scope();
    $scope.$apply(function() {
        $scope.myValue = false;
    });
    
    if ($('#accordion').html('')) {
        $('#accordion').html('<div id="albumpbar" class="progress">' +
            '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%"></div></div>');
    }

    if ($('#postsRow').html('')) {
        $('#postsRow').html('<div id="albumpbar" class="progress">' +
            '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%"></div></div>');
    }

    $('#albumpbar').css('display', 'block');
    $('#postspbar').css('display', 'block');
    userID = idObject;
    console.log(userID);

    if (tabselect !== "fav") {
        
        clickedRow = Object2.parentNode.previousSibling.firstChild.firstChild;

        console.log(clickedRow);
    } else {
        clickedRow = "";
        clickedFavtype = Object2.parentNode.previousSibling.previousSibling.innerHTML;
        console.log(clickedFavtype);
    }
    detailsElement = 1;

    var a = Object2.firstChild;
    var b = a.className;
    console.log(b)
    var testStuff = [];



    var new1 = $("<button class='btn btn-default' onClick='testBackCall();return false;'><span class='glyphicon glyphicon-chevron-left'></span> Back</button ><table border=1><tr><td>Hello World</td><tr><td>You have requested details for User : " + idObject + "</td></table> ");

    $.ajax({
        type: 'GET',
        url: 'http://fb-test-app-new.appspot.com/main.php?',
        data: {
            idValue: userID,
            type: tabselect,
            latitude: plat,
            longitude: plng,
            distance: pdis,
            favType: clickedFavtype
        },
        dataType: 'json',
        success: function(data) {


            console.log(data);
            console.log(data.name);


            if (localStorage.length > 0)

            {
                var favArray = [];

                for (var arrVal = 0, len = localStorage.length; arrVal < len; ++arrVal) {
                    favArray[arrVal] = localStorage.key(arrVal);
                }

                if (favArray.includes(userID.toString())) {

                    var detailFav = $('#favbtnDetail').children("span").attr('class');

                    var jsObj = {
                        "uid": userID,
                        "uname": data.name,
                        "picURL": data.picture.data.url
                    }
                    var strJs = JSON.stringify(jsObj);

                    $("#favbtnDetail").closest('button').attr('onClick', 'favbtnDetailClick(' + strJs + ')')
                    $("#fbpostbtn").closest('button').attr('onClick', 'fbPostClick(' + strJs + ')')



                    $("#favbtnDetail").children('span').attr('class', 'glyphicon glyphicon-star bis');


                } else {

                    var detailFav = $('#favbtnDetail').children("span").attr('class');
                    var jsObj = {
                        "uid": userID,
                        "uname": data.name,
                        "picURL": data.picture.data.url
                    }
                    var strJs = JSON.stringify(jsObj);
                    $("#favbtnDetail").closest('button').attr('onClick', 'favbtnDetailClick(' + strJs + ')')
                    $("#fbpostbtn").closest('button').attr('onClick', 'fbPostClick(' + strJs + ')')
                    $("#favbtnDetail").children('span').attr('class', 'glyphicon glyphicon-star-empty');

                }
            } else {

                var detailFav = $('#favbtnDetail').children("span").attr('class');

                var jsObj = {
                    "uid": userID,
                    "uname": data.name,
                    "picURL": data.picture.data.url
                }
                var strJs = JSON.stringify(jsObj);

                $("#favbtnDetail").closest('button').attr('onClick', 'favbtnDetailClick(' + strJs + ')')
                $("#fbpostbtn").closest('button').attr('onClick', 'fbPostClick(' + strJs + ')')
                $("#favbtnDetail").children('span').attr('class', 'glyphicon glyphicon-star-empty');


            }




            var newAlbums = "";
            var divIDs = ["0", "1", "2", "3", "4"];



            if (typeof data.albums !== "undefined") {

                for (var j = 0; j < data.albums.data.length; j++) {
                    if (typeof data.albums.data[j].photos !== "undefined") {
                        for (var k = 0; k < data.albums.data[j].photos.data.length; k++) {


                            if (data.albums.data[j].photos.data.length == 1) {
                                if (typeof data.albums.data[j].photos.data[0].id !== "undefined") {
                                    var imgID = data.albums.data[j].photos.data[0].id;
                                    var imgSrcAlbum0 = "https://graph.facebook.com/v2.8/" + imgID + "/picture?type(large)&access_token=EAAbAOdJ2ypcBACMHTcUzKlKMPK0MFt55QCKp1p32rZA9kcZB8PHvfEka72ar4YiXB1bGtUFCN9ThZCMvBls8EyPQ1ZAUGCbblygU5ZCVZCFPZCqfz7tUeZAdrEeDJ8StcmSJFo2PZAUcRBtG550DPoI7jOZCMSPHVAeD8ZD";
                                    var img0tag = '<img src = ' + imgSrcAlbum0 + ' class="img-responsive img-rounded" ></img>';
                                    var img1tag = "";
                                } else {
                                    var img0tag = "";
                                }
                            } else {

                                if (k == 0) {
                                    if (typeof data.albums.data[j].photos.data[k].id !== "undefined") {
                                        var imgID = data.albums.data[j].photos.data[k].id;
                                        var imgSrcAlbum0 = "https://graph.facebook.com/v2.8/" + imgID + "/picture?type(large)&access_token=EAAbAOdJ2ypcBACMHTcUzKlKMPK0MFt55QCKp1p32rZA9kcZB8PHvfEka72ar4YiXB1bGtUFCN9ThZCMvBls8EyPQ1ZAUGCbblygU5ZCVZCFPZCqfz7tUeZAdrEeDJ8StcmSJFo2PZAUcRBtG550DPoI7jOZCMSPHVAeD8ZD";
                                        var img0tag = '<img src = ' + imgSrcAlbum0 + ' class="img-responsive img-rounded" ></img>';
                                    } else {
                                        var img0tag = "";
                                    }


                                }


                                if (k != 0) {
                                    if (typeof data.albums.data[j].photos.data[k].id !== "undefined") {
                                        var imgID = data.albums.data[j].photos.data[k].id;
                                        var imgSrcAlbum1 = "https://graph.facebook.com/v2.8/" + imgID + "/picture?type(large)&access_token=EAAbAOdJ2ypcBACMHTcUzKlKMPK0MFt55QCKp1p32rZA9kcZB8PHvfEka72ar4YiXB1bGtUFCN9ThZCMvBls8EyPQ1ZAUGCbblygU5ZCVZCFPZCqfz7tUeZAdrEeDJ8StcmSJFo2PZAUcRBtG550DPoI7jOZCMSPHVAeD8ZD";

                                        var img1tag = '<img src = ' + imgSrcAlbum1 + ' class="img-responsive img-rounded" ></img>';
                                    } else {
                                        var img1tag = "";
                                    }
                                }
                            }
                        }
                    } else {
                        var img0tag = "";
                        var img1tag = "";

                    }


                    console.log(divIDs[j]);
                    var albumPics1 = '<div id="albumData" class="panel panel-default">' +
                        '<div class="panel-heading">' +
                        '<h3 class="panel-title">' +
                        '<a data-toggle="collapse" data-parent="#accordion" href="' + '#' + divIDs[j] + '">' + data.albums.data[j].name + '</a>' +
                        '</h3>' +
                        '</div>' +
                        '<div id="' + divIDs[j] + '" class="panel-collapse collapse">' +

                        '<div class="panel-body">' + img0tag + '</div>' +
                        '<div class="panel-body">' + img1tag + '</div>' +

                        '</div>' +
                        '</div>';




                    newAlbums += albumPics1;



                }
            } else {
                var albumPics1 = '<div class="panel panel-warning">' +
                    '<div class="panel-heading">No data found' +

                    '</div>' +

                    '</div>';
                newAlbums += albumPics1;
            }

            $('#albumpbar').css('display', 'none')
            $('#accordion').html(newAlbums);

            $('#0').removeClass('panel-collapse collapse');
            $('#0').addClass('panel-collapse collapse in');




            var new2 = "";
            var newPosts = "";

            var postorStory = "";

            if (typeof data.posts !== "undefined") {

                console.log(data.posts.data.length);


                for (i = 0; i < data.posts.data.length; i++) {
                    timevalue = moment(data.posts.data[i].created_time).format(('YYYY-MM-DD, hh:mm:ss a'));
                    if (typeof data.posts.data[i].message !== "undefined") {
                        postorStory = data.posts.data[i].message;

                    } else if (typeof data.posts.data[i].story !== "undefined") {
                        postorStory = data.posts.data[i].story;
                    } else {
                        postorStory = "";
                    }


                    /*var new2 = '<div class="panel panel-default"><div class="panel-heading">' +
                        '<div class="row">' +
                        '<div class="col-xs-5 col-md-2 col-sm-2">' +
                        '<img src = "' + data.picture.data.url + '" class="img-thumbnail" ></img></div>' +
                        '<div  class="col-xs-7 col-md-10 col-sm-10 style="display:inline;"><b>' + data.name +
                        '</b><p style="font-size:1em;color:grey; ">' + timevalue + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="panel-body">' +
                        '<div>' + postorStory + '</div>' +
                        '</div>' +
                        '</div></div>';*/
                    
                    
                    var new2 =                       
                        '<div class="panel panel-default">'+
                            '<div class="panel-heading" style="background-color:white">'+
                                '<img src = "' + data.picture.data.url + '" class="img-responsive left" width="40px" height="40px"/>'+
                                   '<div style="padding-left:4em">'+
                                        '<b style="display:inline">' + data.name +'</b><br/>'+
                                        '<p>'+timevalue+'</p>'+
                                    '</div>'+                            
                                    '<div class="panel-body">'+ postorStory +'' +                                          
                                    '</div>' +
                        '</div></div>';          


                    newPosts += new2;


                }
            } else {
                var new2 = '<div class="panel panel-warning">' +
                    '<div class="panel-heading">No data found' +

                    '</div>' +

                    '</div>';
                newPosts += new2;
            }


            $('#postspbar').css('display', 'none');
            $('#postsRow').html(newPosts);
             
          
        
        },

    });



    
   
    
}




function showPhotos(Object) {
    $(Object.id).closest()

    objID = Object.id;
    console.log(Object.id);

}


function gotoNext(prevornext) {

    $('#mainTableDiv').html('')
    $('#test').html('')
    $('#mainTableDiv').css('height', '40em')

    if ($('#mainTableDiv').html('')) {
        $('#mainTableDiv').css('display', 'block')
        $('#mainTableDiv').html('<div id="maintablepbar" class="container progress">' +
            '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%" style = "position: absolute;top:0;bottom: 0;left: 0;right: 0;margin: auto;"></div></div>');
    }

    nextcount = "";
    prevcount = "";
    if (prevornext == "prev") {
        pageURL = prevPage;
    } else {
        pageURL = nextPage;
    }


    $.ajax({
        type: 'GET',
        url: 'http://fb-test-app-new.appspot.com/main.php?',
        data: {
            url: pageURL,
            type: tabselect,
            latitude: plat,
            longitude: plng,
            distance: pdis
        },
        dataType: 'json',
        success: function(data) {

            console.log(data);

            if (typeof data.paging != "undefined") {


                if (typeof data.paging.next != "undefined") {



                    nextButton = "<button type='button' class='btn btn-default' onclick='gotoNext(" + next + ");return false;'>Next</button>"
                    nextcount = "y";
                    nextPage = data.paging.next;

                    if (typeof data.paging.previous != "undefined") {
                        prevPage = data.paging.previous;
                        console.log(prevPage);
                        prevButton = "<button type='button' class='btn btn-default' onclick='gotoNext(" + prev + ");return false;'>Previous</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type='button' class='btn btn-default' onclick='gotoNext(" + next + ");return false;'>Next</button>"
                        prevcount = "y";
                    } else {
                        prevcount = "n";
                        console.log("no previous page");
                    }


                } else {
                    nextcount = "n";
                    console.log("no paging");
                }


            } else {
                nextcount = "n";
                console.log("no paging");

            }



            if (localStorage.length > 0) {
                var favArray = [];

                for (var arrVal = 0, len = localStorage.length; arrVal < len; ++arrVal) {
                    favArray[arrVal] = localStorage.key(arrVal);
                }

            }

            var table = $('<table class="table table-hover"><thead><tr><th>#</th><th>Profile Photo</th><th>Name</th><th>Favorite</th><th>Details</th></tr></thead><tbody>');



            for (var i = 0, count = 1; i < data.data.length; i++, count++) {
                var imgSrc1 = data.data[i].picture.data.url

                var row = $('<tr>').addClass('bar')

                for (var j = 0; j < 1; j++) {



                    var img = $('<img id="newImagedynamic">'); //Equivalent: $(document.createElement('img'))
                    img.attr('src', imgSrc1);
                    img.attr('class', 'img-circle');
                    img.attr('width', '30px');
                    img.attr('height', '30px');
                    img.attr('alt', 'Profile photo');

                    var tdcount = $('<td>').text(count);
                    var tdImg = $('<td>').append(img);
                    var tdName = $('<td>').text(data.data[i].name);

                    if (localStorage.length > 0) {
                        if (favArray.includes(data.data[i].id)) {

                            {
                                var tdDummyID = $('<td>').append('<button id="favbtn" onclick="getUserID(this,' + data.data[i].id + ');return false;" type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-star bis"></span></button>');
                            }

                        } else {
                            var tdDummyID = $('<td>').append('<button id="favbtn" onclick="getUserID(this,' + data.data[i].id + ');return false;" type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-star-empty"></span></button>');
                        }

                    } else {
                        var tdDummyID = $('<td>').append('<button id="favbtn" onclick="getUserID(this,' + data.data[i].id + ');return false;" type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-star-empty"></span></button>');
                    }




                    var tdDetails = $('<td>').append("<button type='button' ng-click='myValue=false;' class='btn btn-default btn-lg' onClick='testDetailsCall(this," + data.data[i].id + ");return false;'><span class='glyphicon glyphicon-chevron-right''></span></button>");

                }
                tdcount.appendTo(row);
                tdImg.appendTo(row);
                tdName.appendTo(row);
                tdDummyID.appendTo(row);
                tdDetails.appendTo(row);

                row.appendTo(table);

            }




            nextButton = "<button type='button' class='btn btn-default' onclick='gotoNext(" + next + ");return false;'>Next</button>";


            $('#maintablepbar').css('display', 'none');
            $('#mainTableDiv').css('height', '');
            $('#mainTableDiv').html(table);
            $('#mainTableDiv').css('display', 'block');

            if (nextcount != "n") {

                if (prevcount != "n") {
                    $('#mainTableDiv').css('height', 'auto');
                    $('#test').css('display', 'block');
                    $('#test').html(prevButton);
                } else {
                    $('#mainTableDiv').css('height', 'auto');
                    $('#test').css('display', 'block');
                    $('#test').html(nextButton);


                }
            } else {
                $('#test').css('display', 'none');
            }

        }
    });
}




$(document).ready(function() {
    $('#inputSearchText').val("");
    clicked = "";
});