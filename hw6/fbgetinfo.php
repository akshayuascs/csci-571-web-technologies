<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$op = array();
$articles = array();
global $distance, $location;
?>

<html>
<?php require_once __DIR__ . '/php-graph-sdk-5.0.0/src/Facebook/autoload.php'; ?>
<?php //require_once __DIR__ . '\..\apps\phpmyadmin4.6.4\vendor\autoload.php'; ?>


<head>
    <style>
        h1 {
            text-align: center;
        }

        #htmlTable {
            width: 50%;
            background-color: whitesmoke;
            font-family: serif;
        }

        td {
            border: none;
        }
    </style>
</head>

<body>
<form method="post" id="myform" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <table id="htmlTable" align="center" border="2">
        <tr>
            <td colspan="5">
                <h1><i>Facebook Search</i></h1>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr>
            </td>
        </tr>
        <tr>
            <td>Keyword:</td>
            <td>
                <input type="text" id="keytext" name="keyword" required title="This can't be left empty" 
                       oninvalid="this.setCustomValidity('This cant be left empty')"
    oninput="setCustomValidity('')" value="<?php if (isset($_POST['keyword'])) {
                    echo $_POST['keyword'];
                } elseif (!empty($_SERVER['QUERY_STRING'])) {
                    global $op;
                    parse_str($_SERVER['QUERY_STRING'], $op);
                    echo $op['hkeyword'];
                }
                ?>">

            </td>

        </tr>
        <tr>
            <td>Type:</td>
            <td>
                <select name="type" id="typeSelect" autocomplete="Off" onchange="displayEle()">
                    <!--using autocomplete = Off as FireFox did not load selected option on refresh; weird on Moz-->
                    <option id="user" <?php if (isset($_POST['type'])) {
                        if ($_POST['type'] == "Users") {
                            echo "selected";
                        }

                    } elseif (!empty($_SERVER['QUERY_STRING'])) {
                        parse_str($_SERVER['QUERY_STRING'], $op);
                        if ($op['htype'] == "user") {
                            echo "selected";
                        }
                    } ?>>Users
                    </option>
                    <option id="page" <?php if (isset($_POST['type'])) {
                        if ($_POST['type'] == "Pages") {
                            echo "selected";
                        }
                    } elseif (!empty($_SERVER['QUERY_STRING'])) {
                        parse_str($_SERVER['QUERY_STRING'], $op);
                        if ($op['htype'] == "page") {
                            echo "selected";
                        }
                    }
                    ?> >Pages
                    </option>
                    <option id="event" <?php if (isset($_POST['type'])) {
                        if ($_POST['type'] == "Events") {
                            echo "selected";
                        }
                    } elseif (!empty($_SERVER['QUERY_STRING'])) {
                        parse_str($_SERVER['QUERY_STRING'], $op);
                        if ($op['htype'] == "event") {
                            echo "selected";
                        }
                    }
                    ?> >Events
                    </option>
                    <option id="place" <?php if (isset($_POST['type'])) {
                        if ($_POST['type'] == "Places") {
                            echo "selected";
                        }
                    } elseif (!empty($_SERVER['QUERY_STRING'])) {
                        parse_str($_SERVER['QUERY_STRING'], $op);
                        if ($op['htype'] == "place") {
                            echo "selected";
                        }
                    }
                    ?> >Places
                    </option>
                    <option id="group" <?php if (isset($_POST['type'])) {
                        if ($_POST['type'] == "Groups") {
                            echo "selected";
                        }
                    } elseif (!empty($_SERVER['QUERY_STRING'])) {
                        parse_str($_SERVER['QUERY_STRING'], $op);
                        if ($op['htype'] == "group") {
                            echo "selected";
                        }
                    }
                    ?> >Groups
                    </option>
                </select>
            </td>
        </tr>
        <!--style="visibility:hidden;"-->
        <tr id="third" style="visibility:hidden;">
            <td>Location</td>
            <td>
                <input type="text" name="location" id="locationid"
                       value="<?php
                       if (isset($_POST['location'])) {
                           echo $_POST['location'];
                       } elseif (!empty($_SERVER['QUERY_STRING'])) {
                           parse_str($_SERVER['QUERY_STRING'], $op);
                           if ($op['htype'] == "place") {
                               if (isset($op['hloc'])) {
                                   echo $op['hloc'];
                               }
                           }
                       }
                       ?>"> Distance(meters)
                <input type="text" id="distanceid" name="distance" value="<?php if (isset($_POST['distance'])) {
                    echo $_POST['distance'];
                } elseif (!empty($_SERVER['QUERY_STRING'])) {
                    parse_str($_SERVER['QUERY_STRING'], $op);
                    if ($op['htype'] == "place") {
                        if (isset($op['hdis'])) {
                            echo $op['hdis'];
                        }
                    }
                }
                ?>">
            </td>

        </tr>
        <tr>
            <td></td>
            <td>
                <input type="submit" value="Search" name="submit">
                <input type="reset" value="Clear" onclick="clearEle(); return false;">
            </td>
        </tr>
    </table>
    <input type="hidden" name="hidval" id="hid" value="">
    <input type="hidden" name="hkeyword" id="hkey" value="<?php if (isset($_POST['keyword'])) {
        echo $_POST['keyword'];
    } ?>">
    <input type="hidden" name="htype" id="hts" value="<?php if (isset($_POST['type'])) {
        echo $_POST['type'];
    } ?>">
</form>
<div id="tableDiv" style="display:none"></div>

<div id="tableDivData" style="display:none"></div>

<div id="tableDiv1" style="display:none"></div>

<div id="postsdisplay" style="display:none;"></div><br><br>
<script>
    function displayEle() {
        var select = document.getElementById("typeSelect").value;
        if (select == "Places") {
            document.getElementById("third").style.visibility = "visible";
        } else {
            document.getElementById("third").style.visibility = "hidden";
        }
    }
    function clearEle() {
        document.getElementById("third").style.visibility = "hidden";
        document.getElementById("keytext").setAttribute("value", "");
        document.getElementById("typeSelect").selectedIndex = "0";
        if (document.getElementById('resultTable')) {
            document.getElementById('resultTable').style.display = "none";
        }
        if (document.getElementById('tableDiv')) {
            document.getElementById('tableDiv').style.display = "none";
        }
        if (document.getElementById('tableDiv1')) {
            document.getElementById('tableDiv1').style.display = "none";
        }
        if (document.getElementById('postsTable')) {
            document.getElementById('postsTable').style.display = "none";
        }
        if (document.getElementById('albumsTable')) {
            document.getElementById('albumsTable').style.display = "none";
        }
        if (document.getElementById('postsdtable')) {
            document.getElementById('postsdtable').style.display = "none";
        }
        if (document.getElementById('albumsdtable')) {
            document.getElementById('albumsdtable').style.display = "none";
        }
        if (document.getElementById('distanceid')) {
            document.getElementById('distanceid').setAttribute("value", "");
        }
        if (document.getElementById('locationid')) {
            document.getElementById('locationid').setAttribute("value", "");
        }
        document.getElementById("hid").setAttribute("value", "");
        document.getElementById("hkey").setAttribute("value", "");
        document.getElementById("hts").setAttribute("value", "");
    }

    <?php
    global $albumTable, $postTable;
    $albumTable = "";
    $postTable = "";
    ?>

    var globalvarid = "";
    var globalvarkey = "";
    var globalvartype = "";
    var globalvarlocation = "";
    var globalvardistance = "";

    function displayAlbums1() {
        document.getElementById("tableDiv").innerHTML = "<?php echo $albumTable; ?>";
    }

    function displayPosts1() {
        document.getElementById("tableDiv1").innerHTML = "<?php echo $postTable; ?>";
    }

    function displayDetails(idvar, keyvar, typevar, locationvar, distancevar) {
        document.getElementById('resultTable').style.display = "none";
        globalvarid = idvar;
        globalvarkey = keyvar;
        globalvartype = typevar;
        globalvarlocation = locationvar;
        globalvardistance = distancevar;
        getID(globalvarid, globalvarkey, globalvartype, globalvarlocation, globalvardistance);
    }

    function displayAlbums() {
         var q = document.getElementById('albumsdtable');        
         var p = document.getElementById('postsdtable');        
        if (q.style.display == 'none') {
            q.style.display = 'block';
            p.style.display = 'none';
        } else {
            q.style.display = 'none';
        }
    }

    function displayPosts() {
        var p = document.getElementById('postsdtable');
        var q = document.getElementById('albumsdtable');
        if (p.style.display == 'none') {
            p.style.display = 'block';
            q.style.display = 'none';
        } else {
            p.style.display = 'none';
        }
    }

    function getID(id, key, type, location, distance) {
        var method = "get"; // Set method to post by default if not specified.
        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", "/fbgetinfo.php");
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "hidval");
        hiddenField.setAttribute("value", id);
        var hiddenField1 = document.createElement("input");
        hiddenField1.setAttribute("type", "hidden");
        hiddenField1.setAttribute("name", "hkeyword");
        hiddenField1.setAttribute("value", key);
        var hiddenField2 = document.createElement("input");
        hiddenField2.setAttribute("type", "hidden");
        hiddenField2.setAttribute("name", "htype");
        hiddenField2.setAttribute("value", type);
        if (type == "place") {
            var hiddenField3 = document.createElement("input");
            hiddenField3.setAttribute("type", "hidden");
            hiddenField3.setAttribute("name", "hloc");
            hiddenField3.setAttribute("value", location);
            var hiddenField4 = document.createElement("input");
            hiddenField4.setAttribute("type", "hidden");
            hiddenField4.setAttribute("name", "hdis");
            hiddenField4.setAttribute("value", distance);
            form.appendChild(hiddenField3);
            form.appendChild(hiddenField4);

        }
        form.appendChild(hiddenField);
        form.appendChild(hiddenField1);
        form.appendChild(hiddenField2);
        document.body.appendChild(form);
        form.submit();
    }

    function display1Album(element) {
        var row = element.parentNode.parentNode.rowIndex;
        //alert(row);
        if (row == "0") {
            if (typeof row + 1 != "undefined") {
                var t0 = document.getElementById("talbumrow0");
                if (t0.style.display == "none") {
                    t0.style.display = "block";
                }
                else {
                    t0.style.display = "none";
                }
            }
        }
        else if (row == "2") {
            if (typeof row + 1 != "undefined") {
                var t1 = document.getElementById("talbumrow1");
                if (t1.style.display == "none") {
                    t1.style.display = "block";
                }
                else {
                    t1.style.display = "none";
                }
            }
        }
        else if (row == "4") {
            if (typeof row + 1 != "undefined") {
                var t2 = document.getElementById("talbumrow2");
                if (t2.style.display == "none") {
                    t2.style.display = "block";
                }
                else {
                    t2.style.display = "none";
                }
            }
        }
        else if (row == "6") {
            if (typeof row + 1 != "undefined") {
                var t3 = document.getElementById("talbumrow3");
                if (t3.style.display == "none") {
                    t3.style.display = "block";
                }
                else {
                    t3.style.display = "none";
                }
            }
        }
        else if (row == "8") {
            if (typeof row + 1 != "undefined") {
                var t4 = document.getElementById("talbumrow4");
                if (t4.style.display == "none") {
                    t4.style.display = "block";
                }
                else {
                    t4.style.display = "none";
                }
            }
        }
    }

    function openImage(imgURL) {
        var myWindow = window.open();
        var inputImg = myWindow.document.createElement('input');
        inputImg.type = "image";
        inputImg.src = imgURL;
        inputImg.value = "";
        myWindow.document.body.appendChild(inputImg);
    }

</script>
<?php
    
function buildTable()
{
    global $distance, $location;
    global $glat, $glng;
    $glat = "";
    $glng = "";
    $q = $_POST['keyword'];
    if (isset($_POST['type'])) {
        if ($_POST['type'] == "Users") {
            $type = "user";
        } elseif ($_POST['type'] == "Pages") {
            $type = "page";
        } elseif ($_POST['type'] == "Events") {
            $type = "event";
        } elseif ($_POST['type'] == "Places") {
            $type = "place";
            $location = $_POST['location'];
            $distance = $_POST['distance'];
        } else {
            $type = "group";
        }
    }
    if ($type == "place") {
        if ($location != "") {
            $gurl = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($location) . "&key=AIzaSyDoM0rvIh9qBQX-1LVvAN7HcANBYTXnlxQ";
            $gurlcontents = file_get_contents($gurl);
            $gurllatlngt = json_decode($gurlcontents);
            if (isset($gurllatlngt->results[0]->geometry->location->lat))
                $glat = $gurllatlngt->results[0]->geometry->location->lat;
            else {
                $glat = "undef";
            }
            if (isset($gurllatlngt->results[0]->geometry->location->lng))
                $glng = $gurllatlngt->results[0]->geometry->location->lng;
            else {
                $glng = "undef";
            }
        }
    }
    //echo $glat."<br>".$glng;
    
    $fields = "id,name,picture.width(700).height(700)";
    $aToken = "EAAbAOdJ2ypcBACMHTcUzKlKMPK0MFt55QCKp1p32rZA9kcZB8PHvfEka72ar4YiXB1bGtUFCN9ThZCMvBls8EyPQ1ZAUGCbblygU5ZCVZCFPZCqfz7tUeZAdrEeDJ8StcmSJFo2PZAUcRBtG550DPoI7jOZCMSPHVAeD8ZD";
    if ($type != "place") {
        if ($type == "event") {
            $url = "search?q=" . urlencode($q) . "&type=$type&fields=$fields,place";
        } else {
            $url = "search?q=" . urlencode($q) . "&type=$type&fields=$fields";
        }
    } else {
        if (($glat != "undef" && $glng != "undef")) {
            if ($glat != "" && $glng != "") {
                $url = "search?q=" . urlencode($q) . "&type=place&center=$glat,$glng&distance=$distance&fields=$fields";
            }
            else {
                $url = "search?q=" . urlencode($q) . "&type=$type&fields=$fields";
            }
        }
        else {
            echo "<table id='resultTable' class='finalTable' align='center' border='1' style='background-color: whitesmoke; border-color:darkgrey; '><tr><td align='center' width='800em'>Address is invalid</td></tr></table>";
            exit();
            //$url = "search?q=" . urlencode($q) . "&type=$type&fields=$fields";
        }
    }
    urlencode($url);
    $string = call_graphAPI($url); //file_get_contents($url);
    global $index;
    $index = 0;
    $items = array();
    //var_dump($string);
    if(count($string['data'])!=0)
    {
 
        
        if ($type != "event") {
            $th1 = "<th align='left'>" . "Profile Photo" . "</th>";
            $th2 = "<th align='left'>" . "Name" . "</th>";
            $th3 = "<th align='left'>" . "Details" . "</th>";
            echo "<table id='resultTable' class='finalTable' align='center' border='1' style='background-color: whitesmoke; border-collapse: collapse;'>";
            echo "<tr>";
            echo $th1;
            echo $th2;
            echo $th3;
            
                    for($i=0;$i<count($string['data']);$i++)
        {
                if ($type != "place") {
                    $anewpg = "<td width='100em'
style='border:1px solid;'>" . " " . '<a href="#"  onclick="displayDetails(' . "'" . $string['data'][$i]['id'] . "', '" . $q . "', '" . $type . "','0','0'" . ');">Details</a>' . "</td>";
                } else {
                    $anewpg = "<td width='100em'
style='border:1px solid;'>" . " " . '<a href="#"  onclick="displayDetails(' . "'" . $string['data'][$i]['id'] . "', '" . $q . "', '" . $type . "', '" . $location . "', '" . $distance . "'" . ');">Details</a>' . "</td>";
                }
                $newImg = $string['data'][$i]['picture']['data']['url'];
                //$imageData = base64_encode(file_get_contents($newImg));
                echo "<tr >";
                echo "<td width='200em' style='border:1px solid;'>" . '<a href="#" onClick="openImage(' . "'" . $newImg . "'" . ');">
                    
                <img src="' . $newImg . '" height="40" width="30"></a>' . "</td>";
                echo "<td  width='500em'
style='border:1px solid;' >" . " " . $string['data'][$i]['name'] . " " . "</td>";
                echo $anewpg;
                echo "</tr>";
                //global $index;
                //$index = $index + 1;
            }
        } else {
            $th1 = "<th align='left'>" . "Profile Photo" . "</th>";
            $th2 = "<th align='left'>" . "Name" . "</th>";
            $th3 = "<th align='left'>" . "Place" . "</th>";
            echo "<table id='resultTable' class='finalTable' align='center' border='1' style='background-color: whitesmoke; border-collapse: collapse;'>";
            echo "<tr>";
            echo $th1;
            echo $th2;
            echo $th3;
             for($i=0;$i<count($string['data']);$i++)
        {
            //foreach ($json_o->data as $p) {
                //$items[] = $p->id;
                if (!empty($string['data'][$i]['place']['name'])) {
                    $anewpg = "<td width='200em' style='border:1px solid;'>" . " " . $string['data'][$i]['place']['name'] . "</td>";
                } else {
                    $anewpg = "<td width='200em' style='border:1px solid;'>" . " " . "</td>";
                }
                $newImg = $string['data'][$i]['picture']['data']['url'];
                //$imageData = base64_encode(file_get_contents($newImg));
                echo "<tr >";
                echo "<td width='200em' style='border:1px solid;'>" . '<a href="#" onClick="openImage(' . "'" . $newImg . "'" . ');">
                    
                <img src="' . $newImg . '" height="40" width="40"></a>' . "</td>";
                echo "<td  width='400em'
style='border:1px solid;' >" . " " . $string['data'][$i]['name'] . " " . "</td>";
                echo $anewpg;
                echo "</tr>";
            }
        }
    }
    else {
        echo "<table id='resultTable' class='finalTable' align='center' border='1' style='background-color: whitesmoke; border-color:darkgrey; '><tr><td align='center' width='800em'>No Records has been found</td></tr></table>";
    }

    if (isset($_POST["hidval"])) {
        $bval = $_POST["hidval"];
    }
}

$albumTable = "";
$postTable = "";

function buildResults()
{
    global $idval;
    global $aToken;
    global $albumTable, $postTable;
    global $typeval;
    $testtxt = "";
    //if ($typeval == "page" || $typeval == "group" || $typeval == "user" || $typeval == "place") {
        $url2 = "$idval?fields=id,name,picture.width(700).height(700),albums.limit(5){name,photos.limit(2){name,picture}},posts.limit(5)";
    //} elseif ($typeval == "event") {
        //$url2 = "https://graph.facebook.com/v2.8/$idval?fields=id,name,picture.width(700).height(700),posts.limit(5)&access_token=$aToken";
    //}
    $string2 = call_graphAPI($url2);
    //var_dump($string2);
    //$json_o2 = json_decode($string2);
    /*for albums*/

    if( isset($string2['albums']['data']) && count($string2['albums']['data'])!=0)
    {
        $albumTable = "<table width='800px' id='albumsTable' align='center' border='1' style='background-color: lightgrey;border-color:darkgrey; '><tr><td  align='center' ><a href='javascript:void(0);' onclick='displayAlbums()'>Albums</a></td></tr></table><br>";
        echo $albumTable;
        echo "<script>displayAlbums1();</script>";
        $albumsDisplay = "<table id='albumsdtable'  width='800px' align='center' border='1' style='display:none;  font-family:serif; border-collapse: collapse; border-color:gray;'>";
        echo $albumsDisplay;
            
        for($i=0;$i<count($string2['albums']['data']);$i++)
        {
                if(!empty($string2['albums']['data'][$i]['name']))
                {
                    //echo $string2['albums']['data'][$i]['name']."<br>";
                    
                    for($ab=0,$albumsCount=0;$ab<5;$ab++)
                    {
                        if(!empty($string2['albums']['data'][$i]['photos']['data'][$ab]['picture'])) 
                        {
                            $albumsCount++;
                        }
                    }
                    
                
                $altxt[] = $string2['albums']['data'][$i]['name'];
                //if(!empty($string2['albums']['data'][$i]['photos']['data'][0]['picture']))  //(isset($json_o2->albums->data[$x]->photos->data[0]->picture)) //if(isset($altxt[$x]))
                if($albumsCount!=0)    
                {
                    echo "<tr><td width='800px' style='border:1px solid; border-color:gray;'>" . "<a href=javascript:void(0); onclick='display1Album(this);'>";
                } else {
                    echo "<tr><td width='800px' style='border:1px solid; border-color:gray;'>";
                }
                echo $altxt[$i];
                echo "</a>" . "</td>" . "</tr>";
                }
            echo "<tr id='talbumrow$i' style='display:none;' >";
            for ($j = 0; $j < 2; $j++) {
                if(!empty($string2['albums']['data'][$i]['photos']['data'][$j]['picture'])) //(isset($json_o2->albums->data[$x]->photos->data[$y]->picture)) 
                {
                    if (!empty($string2['albums']['data'][$i]['photos']['data'][$j]['picture'])) $altxt1[$j] = $string2['albums']['data'][$i]['photos']['data'][$j]['picture'];
                    if (!empty($string2['albums']['data'][$i]['photos']['data'][$j]['id']))
                        //isset($json_o2->albums->data[$x]->photos->data[$y]->id)) 
                        $altxt1id[$j] = $string2['albums']['data'][$i]['photos']['data'][$j]['id'];
                    //echo $altxt1id[$j];
                    if (!empty($altxt1id[$j])) {
                        $urlidpic1 = "$altxt1id[$j]/picture?type(large)";
                        $string3 = getPictureURL($urlidpic1);
                       // var_dump($string3);
                        $imageData = $string3;// base64_encode(file_get_contents($altxt1[$y]));
                        echo "<td width='' style='border:1px solid; border-color:gray;'>";
                        echo '<a href="javascript:void(0);" onClick="openImage('."'".$string3."'".'); " ><img src="' . $altxt1[$j] . '" height="80" width="80"></a>';
                        echo "</td>";
                    }
                }
            }
            echo "</tr>";                

        }
echo "</table>";
    }
    else {
        $albumTable = "<table width='800px' id='albumsTable' align='center' border='1' style='background-color: whitesmoke; border-color:darkgrey; '><tr><td  align='center' >No albums has been found</td></tr></table><br>";
        echo $albumTable;
        echo "<script>displayAlbums1();</script>";
    }
    echo "<br>";
    
    for($a=0,$postsCount=0;$a<5;$a++)
    {
        if (!empty($string2['posts']['data'][$a]['message']))
        {
            $postsCount++;
        }
    }
        //echo $postsCount;
    
    if($postsCount!=0 && count($string2['posts']['data'])!=0)
    //if (isset($json_o2->posts->data[0]->message) || isset($json_o2->posts->data[1]->message) || isset($json_o2->posts->data[2]->message) || isset($json_o2->posts->data[3]->message) || isset($json_o2->posts->data[4]->message))
     {
        $postTable = "<table id='postsTable' width='800px' align='center' border='1' style='background-color: lightgrey;border-color:darkgrey; '><tr><td  align='center'><a href='javascript:void(0);' onclick='displayPosts()'>Posts</a></td></tr></table><br>";
        echo $postTable;
        echo "<script>displayPosts1();</script>";
        $th21 = "<th align='left' style=' background-color:whitesmoke; font-family:serif;'>" . "Message" . "</th>";
        $postsDisplay = "<table id='postsdtable'  width='800px' align='center' border='1' style='display:none;  font-family:serif; border-collapse: collapse; border-color:gray;'>";
        echo $postsDisplay;
        echo $th21;
         //var_dump($string2['posts']['data']);
        for ($j = 0; $j < 5; $j++) {
            if (!empty($string2['posts']['data'][$j]['message'])) {
                $txt[$j] = $string2['posts']['data'][$j]['message'];
                if (isset($txt[$j])) {
                    echo "<tr><td width='800px' style='border:1px solid; border-color:gray;'>";
                    echo $txt[$j];
                    echo "</td>";
                    echo "</tr>";
                }
            }
        }
        echo "</table><br><br>";
    } else {
        $postTable = "<table id='postsTable' width='800px' border='1' align='center' style='background-color: whitesmoke; border-color:darkgrey; '><tr><td  align='center'>No Posts has been found</td></tr></table><br>";
        echo $postTable;
        echo "<script>displayPosts1();</script>";
    }
}
if (isset($_POST['submit'])) {
    //var_dump($_POST);
    $keyword = $_POST['keyword'];
    $type = $_POST['type'];
    global $distance, $location;
    if (isset($_POST['type'])) {
        if ($_POST['type'] == "Places") {
            $distance = $_POST['distance'];
            $location = $_POST['location'];
            echo "<script>displayEle();</script>";
        }
    }
    
    if($_POST['type'] == "Places" && $location=="" && $distance!="")
    {
       echo "<table id='resultTable' class='finalTable' align='center' border='1' style='background-color: whitesmoke; border-color:darkgrey; '><tr><td align='center' width='800em'>Distance specified without location or address</td></tr></table>";
    }
    else
    {
         buildTable();    
    }
    
    
}
$idval = "";
global $idval;
if (!empty($_SERVER['QUERY_STRING'])) {

    parse_str($_SERVER['QUERY_STRING'], $op);
    if ($op['htype'] == "place") {
        echo "<script>displayEle();</script>";
    }

    $idval = $op['hidval'];
    $typeval = $op['htype'];
    buildResults();
}
    
 
     /*newSDK code*/
    function call_graphAPI($queryurl) {
        //echo "<br/>Executed query :".$query."<br/>";
        $fb = new Facebook\Facebook
            (['app_id' => '1900204436933271',
    'app_secret' => '6b08289ec45bc284c3884f81ff623078',
    'default_graph_version' => 'v2.8',]);
        $aToken = "EAAbAOdJ2ypcBACMHTcUzKlKMPK0MFt55QCKp1p32rZA9kcZB8PHvfEka72ar4YiXB1bGtUFCN9ThZCMvBls8EyPQ1ZAUGCbblygU5ZCVZCFPZCqfz7tUeZAdrEeDJ8StcmSJFo2PZAUcRBtG550DPoI7jOZCMSPHVAeD8ZD";   

        try {
          // Returns a `Facebook\FacebookResponse` object
          $response = $fb->get($queryurl, $aToken);
            //$responseAlbum = $fb->get('/10155063661262878/picture',$aToken);
            
            
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
        } catch(Facebook\Exceptions $e){
          echo 'Facebook general SDK returned an error: ' . $e->getMessage(); 
        }

      $resultArray = $response->getDecodedBody();
        return $resultArray;
        
    } 
    
    function getPictureURL($picQuery)
    {
        $fb = new Facebook\Facebook
            (['app_id' => '1900204436933271',
    'app_secret' => '6b08289ec45bc284c3884f81ff623078',
    'default_graph_version' => 'v2.8',]);
        $aToken = "EAAbAOdJ2ypcBACMHTcUzKlKMPK0MFt55QCKp1p32rZA9kcZB8PHvfEka72ar4YiXB1bGtUFCN9ThZCMvBls8EyPQ1ZAUGCbblygU5ZCVZCFPZCqfz7tUeZAdrEeDJ8StcmSJFo2PZAUcRBtG550DPoI7jOZCMSPHVAeD8ZD";

        try {
          // Returns a `Facebook\FacebookResponse` object
          //$response = $fb->get($queryurl, $aToken);
            $responseAlbum = $fb->get($picQuery,$aToken);
            
            
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
        } catch(Facebook\Exceptions $e){
          echo 'Facebook general SDK returned an error: ' . $e->getMessage(); 
        }
        
       
        $x = $responseAlbum->getHeaders();

        return $x['Location'];
    }
    
?>
</body>
<noscript>
</html>