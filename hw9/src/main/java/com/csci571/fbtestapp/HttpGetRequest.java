package com.csci571.fbtestapp;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpGetRequest extends AsyncTask<String, Void, String> {
    public static int timeOutVal = 15000;
    public static String reqTypeMethod = "GET";
    public static int readTimeoutVal = 15000;

    @Override
    protected String doInBackground(String... params){
        String outputVal;
        String ipVal;
        String queryURL = params[0];


        try {
            URL newURL = new URL(queryURL);

            HttpURLConnection connection =(HttpURLConnection)
                    newURL.openConnection();

            connection.setRequestMethod(reqTypeMethod);
            connection.setReadTimeout(readTimeoutVal);
            connection.setConnectTimeout(timeOutVal);


            connection.connect();

            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());

            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while((ipVal = reader.readLine()) != null){
                stringBuilder.append(ipVal);
            }

            reader.close();
            streamReader.close();

            outputVal = stringBuilder.toString();
        }
        catch(IOException e){
            Log.d("IO Exception",e.toString());

            outputVal = null;
        } catch (Exception e){

            Log.d("General Exception",e.getMessage());
            outputVal = null;
        }
        return outputVal;
    }



    protected void onPostExecute(String result){
        super.onPostExecute(result);
    }
}