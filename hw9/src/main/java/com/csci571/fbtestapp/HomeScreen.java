package com.csci571.fbtestapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.id.message;
import static android.app.PendingIntent.getActivity;
import static android.app.SearchManager.USER_QUERY;
import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class HomeScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //private static final String SEARCH_KEYWORD = "com.csci571.fbtestapp.searchEditText";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Button searchbtn = (Button) findViewById(R.id.searchBtn);
        searchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText searchEditText = (EditText) findViewById(R.id.searchBar);
                String searchQuery = searchEditText.getText().toString();
                if (searchQuery.matches("")) {
                    Toast.makeText(HomeScreen.this, "Please enter a keyword!", Toast.LENGTH_SHORT).show();
                    return;
                }
                else
                {
                    sendMessage(v);
                }

                //Toast.makeText(HomeScreen.this,
                        //"Search Clicked", Toast.LENGTH_LONG).show();

            }
        });

        Button clearbtn = (Button) findViewById(R.id.clrBtn);
        clearbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView txtview = (TextView) findViewById(R.id.searchBar);
                txtview.setText("");



                //Toast.makeText(HomeScreen.this,
                        //"Clear Clicked", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void sendMessage(View view)
    {
        Intent intent = new Intent(HomeScreen.this, ResultsActivity.class);
        startActivity(intent);
        EditText searchEditText = (EditText) findViewById(R.id.searchBar);
        String searchQuery = searchEditText.getText().toString();
        if(searchQuery!=null && searchQuery.trim().length()!=0){
            intent.putExtra("searchQuery", searchQuery);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

       if (id == R.id.aboutMeActivity) {
           Intent intent = new Intent(this, AboutMeActivity.class);
           startActivity(intent);
            // Handle the camera action
        } else if (id == R.id.homeActivity) {
           Intent intent = new Intent(this, HomeScreen.class);
           startActivity(intent);

        } else if (id == R.id.favActivity) {
           Intent intent = new Intent(this, FavoritesActivity.class);
           startActivity(intent);
//           EditText searchEditText = (EditText) findViewById(R.id.searchBar);
//           String searchQuery = searchEditText.getText().toString();
//           if(searchQuery!=null && searchQuery.trim().length()!=0){
//               intent.putExtra("searchQuery", searchQuery);
//               startActivity(intent);
//           }

        } /*else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }




}
