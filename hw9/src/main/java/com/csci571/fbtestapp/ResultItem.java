package com.csci571.fbtestapp;

/**
 * Created by AKSHAY on 20-Apr-17.
 */

public class ResultItem {

        private String profileid;
        private String profilename;
        private String profileurl;
        private String profiletype;
        private boolean profilefav = false;

        public ResultItem() {
        }

        public ResultItem(String profileid, String profilename, String profileurl) {
            this.profileid = profileid;
            this.profilename = profilename;
            this.profileurl = profileurl;
        }



        public String getProfileid() {

            return profileid;
        }

        public void setProfileid(String profileid) {
            this.profileid = profileid;
        }

        public String getProfilename() {
            return profilename;
        }

        public void setProfilename(String profilepic) {
            this.profilename = profilename;
        }

        public String getProfileurl() {
            return profileurl;
        }

        public void setProfileurl(String profileurl) {
            this.profileurl = profileurl;
        }

        public String getProfiletype() {
            return profiletype;
        }

        public void setProfiletype(String profiletype) {
            this.profiletype = profiletype;
        }

        public boolean isProfilefav() {
            return profilefav;
        }

        public void setProfilefav(boolean profilefav) {
            this.profilefav = profilefav;
        }

    public ResultItem(String profileid, String profilename, String profileurl, String profiletype) {
        this.profileid = profileid;
        this.profilename = profilename;
        this.profileurl = profileurl;
        this.profiletype = profiletype;
    }

}
