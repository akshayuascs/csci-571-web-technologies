package com.csci571.fbtestapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.*;
import java.net.*;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.R.attr.id;
import static android.R.attr.name;
import static android.R.attr.type;


public class ResultsActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static String searchKey;
    public static int timeOutVal = 15000;
    public static String reqTypeMethod = "GET";
    public static int readTimeoutVal = 15000;



    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        //mViewPager.setOffscreenPageLimit(0);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        setupTabIcons();

        Intent intent = getIntent();
        String userSearchQuery = intent.getStringExtra("searchQuery");
        if(userSearchQuery != null)
        {
            searchKey = userSearchQuery;
            Log.d("SearchKeyword",searchKey);
        }
        else
        {
            Log.d("SearchKeyword",searchKey);
        }

        FragmentManager fm = null;
    }

    private void setupTabIcons()
    {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setIcon(R.mipmap.fb_users);
        tabLayout.getTabAt(1).setIcon(R.mipmap.fb_pages);
        tabLayout.getTabAt(2).setIcon(R.mipmap.fb_events);
        tabLayout.getTabAt(3).setIcon(R.mipmap.fb_places);
        tabLayout.getTabAt(4).setIcon(R.mipmap.fb_groups);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putString("searchKeyWord",searchKey);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            //http://fb-app-test.appspot.com/main.php?value=usc&type=user

            View newResultView = inflater.inflate(R.layout.fragment_results, container, false);
            Button nextBtn = (Button) newResultView.findViewById(R.id.nextBtn);
            Button prevBtn = (Button) newResultView.findViewById(R.id.prevBtn);

            Bundle args = getArguments();

            String userSearchkey = "";
            userSearchkey = args.getString("searchKeyWord");

            int selectedTab = args.getInt(ARG_SECTION_NUMBER)-1;
            String callUrl = null;
            String typeSelect = null;

            if(selectedTab == 0) {
                typeSelect = "user";
            } else if(selectedTab == 1) {
                typeSelect = "page";

            } else if(selectedTab == 2 ) {
                typeSelect = "event";

            } else if(selectedTab == 3) {
                typeSelect = "place";

            } else if(selectedTab == 4) {
                typeSelect = "group";

            } else {
                typeSelect = "user";
            }
            if(typeSelect!="place"){
                callUrl = "http://fb-app-test.appspot.com/main.php?value="+userSearchkey+"&type="+typeSelect+"";
            }
            else {
                callUrl = "http://fb-app-test.appspot.com/main.php?value=" + userSearchkey + "&type=" + typeSelect + "&latitude=30&longitude=30";
            }


            ListView resultView = (ListView) newResultView.findViewById(R.id.profilesListView);


            try {
                String serverResult;
                HttpGetRequest getResult = new HttpGetRequest();
                serverResult = getResult.execute(callUrl).get();
                //HttpGetRequest.execute(callUrl).get();
                if(serverResult==null) {

                } else {
                    JSONObject jsonresult1 = new JSONObject(serverResult);
                    List<ResultItem> ListItemArrayNew = new ArrayList<ResultItem>();
                    ResultItem resultValue;
                    if (jsonresult1.has("data")) {
                        Log.d("enter","enter");
                        JSONArray jsonIntData = jsonresult1.getJSONArray("data");
                        for (int len = 0; len < jsonIntData.length(); len++) {

                            JSONObject jsonInt2Data = jsonIntData.getJSONObject(len);
                            JSONObject ppicURL = jsonInt2Data.getJSONObject("picture").getJSONObject("data");
                            String resultname = jsonInt2Data.getString("name");
                            String resulturl = ppicURL.getString("url");
                            String resultid = jsonInt2Data.getString("id");
                            resultValue = new ResultItem(resultid, resultname, resulturl);


                            SharedPreferences retrieveFav = getSharedPreferences(this.getContext());
                            if (retrieveFav.contains(resultValue.getProfileid())) {
                                resultValue.setProfilefav(true);

                            }

                            resultValue.setProfiletype(typeSelect);
                            ListItemArrayNew.add(resultValue);
                        }



                        nextBtn.setVisibility(View.VISIBLE);
                        prevBtn.setVisibility(View.VISIBLE);


                        if(ListItemArrayNew.size()<10){
                            nextBtn.setEnabled(false);
                            prevBtn.setEnabled(false);
                            ListAdapter resultAdapter = new customAdapter(getContext(), R.layout.results_listview, ListItemArrayNew.subList(0,ListItemArrayNew.size()));
                            resultView.setAdapter(resultAdapter);
                        }
                        else{
                            ListAdapter resultAdapter = new customAdapter(getContext(), R.layout.results_listview, ListItemArrayNew.subList(0,10));
                            resultView.setAdapter(resultAdapter);




                        }

                        prevBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {

                            }
                        });

                        nextBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {

                            }
                        });

                    }
                }


            }catch(JSONException e) {
                Log.d("Results",e.getMessage());
                //e.printStackTrace();
            } catch (Exception e){
                Log.d("Results",e.getMessage());
                //e.printStackTrace();
            }

            return newResultView;

        }

        public static SharedPreferences getSharedPreferences (Context context) {
            return context.getSharedPreferences("Favorites", MODE_PRIVATE);
        }


    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 5 total pages.
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Users";
                case 1:
                    return "Pages";
                case 2:
                    return "Events";
                case 3:
                    return "Places";
                case 4:
                    return "Groups";
            }
            return null;
        }
    }
}
