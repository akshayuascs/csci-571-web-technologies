package com.csci571.fbtestapp;

/**
 * Created by AKSHAY on 16-Apr-17.
 */

import java.util.HashMap;
import java.util.List;
import android.content.Context;
import android.graphics.Typeface;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import static com.csci571.fbtestapp.R.id.moreDetailsExpdListView;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context contextNew;
    private List<String> expandableListTitleContents;
    private HashMap<String, List<String>> expandableListDetailContents;
    private String isEmptyAlbum;

    public CustomExpandableListAdapter(Context contextNew, List<String> expandableListTitleContents,
                                       HashMap<String, List<String>> expandableListDetailContents,String isEmptyAlbum) {
        this.contextNew = contextNew;
        this.expandableListTitleContents = expandableListTitleContents;
        this.expandableListDetailContents = expandableListDetailContents;
        this.isEmptyAlbum=isEmptyAlbum;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetailContents.get(this.expandableListTitleContents.get(listPosition))
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.contextNew
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.details_album_photos, null);
        }

        if(isEmptyAlbum==null) {
            ImageView imgHigh1 = (ImageView) convertView.findViewById(R.id.detailsListImg1);
            ImageView imgHigh2 = (ImageView) convertView.findViewById(R.id.detailsListImg2);

            if (imgHigh1 != null && imgHigh2 != null) {
                String position = expandableListTitleContents.get(listPosition);
                List<String> imgLoad = expandableListDetailContents.get(position);
                Picasso.with(contextNew).load(imgLoad.get(0)).into(imgHigh1);
                Picasso.with(contextNew).load(imgLoad.get(1)).into(imgHigh2);
            }
        } else {
            Log.d("albums empty", "Yes");
            //TextView tt1 = (TextView) convertView.findViewById(R.id.EmptyAlbumsOrText);
            //tt1.setText(empty_abums);
        }

        return convertView;
    }



    @Override
    public int getChildrenCount(int listPosition) {
        //return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
        return 1;
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitleContents.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitleContents.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.contextNew.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.results_exp_list, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.results_exp_listHeadRow);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }


}


