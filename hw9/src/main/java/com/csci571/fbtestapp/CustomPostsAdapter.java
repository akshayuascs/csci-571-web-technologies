package com.csci571.fbtestapp;

/**
 * Created by AKSHAY on 16-Apr-17.
 */

import android.content.Context;
import android.view.View;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;

import static android.R.attr.name;
import static android.R.attr.resource;


public class CustomPostsAdapter extends ArrayAdapter<PostData> {

    private String postsName;
    private String postsPicurl;
    private Context contextpostsNew;
    private String isPostEmpty;

    public CustomPostsAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public CustomPostsAdapter(Context contextpostsNew, int resourceNumber, List<PostData> postsNew, String postsName, String postsPicurl, String isPostEmpty) {
        super(contextpostsNew, resourceNumber, postsNew);
        this.contextpostsNew = contextpostsNew;
        this.postsName=postsName;
        this.postsPicurl=postsPicurl;
        this.isPostEmpty=isPostEmpty;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View postsview = convertView;

        if (postsview == null) {
            LayoutInflater viInflate;
            viInflate = LayoutInflater.from(getContext());
            postsview = viInflate.inflate(R.layout.detailspostrows, null);
        }

        final PostData profilePost = getItem(position);

        if (profilePost != null) {
            ImageView profilePic = (ImageView) postsview.findViewById(R.id.detailsRowPostImg);
            TextView profileName = (TextView) postsview.findViewById(R.id.detailsRowPostName);
            TextView postedTime = (TextView) postsview.findViewById(R.id.detailsRowPostTime);
            TextView postMsg = (TextView) postsview.findViewById(R.id.detailsRowPostMsg);

            if (profileName != null) {
                profileName.setText(postsName);
            }

            if (postedTime != null) {
                postedTime.setText(profilePost.getCreated_time());
            }

            if (profilePic!=null){
                Picasso.with(contextpostsNew).load(postsPicurl).into(profilePic);
            }
            if (postMsg != null) {
                postMsg.setText(profilePost.getMessage());
            }


        }




        return postsview;
    }

}
