package com.csci571.fbtestapp;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



import com.squareup.picasso.Picasso;

import java.util.List;

import static android.R.attr.id;
import static android.R.attr.name;
import static android.R.attr.type;

public class customAdapter extends ArrayAdapter<ResultItem> {
    private Context context;



    public customAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public customAdapter(Context context, int resource, List<ResultItem> items) {
        super(context, resource, items);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View initialView = convertView;
        final ResultItem prow = getItem(position);



        if (initialView == null) {
            LayoutInflater view1;
            view1 = LayoutInflater.from(getContext());
            initialView = view1.inflate(R.layout.results_row, null);
        }

        ImageView favColor = (ImageView) initialView.findViewById(R.id.profileStar);
        if (prow != null) {
            ImageView imgV = (ImageView) initialView.findViewById(R.id.profileicon);
            TextView textView1 = (TextView) initialView.findViewById(R.id.profileName);

            if(prow.isProfilefav()) {
                favColor.setImageResource(R.mipmap.fb_favorites_on);
            } else {
                favColor.setImageResource(R.mipmap.fb_favorites_off);
            }

            if (textView1 != null) {
                textView1.setText(prow.getProfilename());
            }

            if (imgV!=null){
                Picasso.with(context).load(prow.getProfileurl()).into(imgV);

            }
        }


        ImageView details_button_ImageView = (ImageView) initialView.findViewById(R.id.profileDetails);

        details_button_ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getContext(), MoreDetailsActivity.class);
                try {
                    String profileName = prow.getProfilename();
                    String profileId = prow.getProfileid();
                    String profileUrl = prow.getProfileurl();
                    String profileType = prow.getProfiletype();
                    //if (profileId != null && profileId.trim().length() != 0) {
                    if (profileId != null) {
                        intent.putExtra("profileN", profileName);
                        intent.putExtra("profileI",profileId);
                        intent.putExtra("profileU",profileUrl);
                        intent.putExtra("profileT",profileType);
                        Log.d("profileName", profileName);
                        Log.d("profileID",profileId);
                        Log.d("profileUrl",profileUrl);
                        Log.d("profileType",profileType);
                        //getContext().startActivity(intent);

                    }
                    getContext().startActivity(intent);
                }catch (NullPointerException e){
                    Log.d("CustomAdapNullPointer ",e.toString());
                    //e.printStackTrace();
                }catch (Exception e){
                    Log.d("CustomAdapterEx",e.toString());
                    //e.printStackTrace();
                }
            }
        });

        return initialView;
    }


}