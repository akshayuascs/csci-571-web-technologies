package com.csci571.fbtestapp;

/**
 * Created by AKSHAY on 16-Apr-17.
 */

public class PostData {

    private String message;
    private String created_time;

    public PostData(String message, String created_time) {
        this.message = message;
        this.created_time = created_time;
    }

    public String getMessage() {

        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }
}
