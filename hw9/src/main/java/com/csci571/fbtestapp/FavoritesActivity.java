package com.csci571.fbtestapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Map;

//import static com.csci571.fbtestapp.R.id.textView;

public class FavoritesActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static ArrayList<ResultItem> favItemsUsers;
    private static ArrayList<ResultItem> favItemsPages;
    private static ArrayList<ResultItem> favItemsEvents;
    private static ArrayList<ResultItem> favItemsGroups;
    private static ArrayList<ResultItem> favItemsPlaces;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        setupTabIcons();

    }

    private void setupTabIcons()
    {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setIcon(R.mipmap.fb_users);
        tabLayout.getTabAt(1).setIcon(R.mipmap.fb_pages);
        tabLayout.getTabAt(2).setIcon(R.mipmap.fb_events);
        tabLayout.getTabAt(3).setIcon(R.mipmap.fb_places);
        tabLayout.getTabAt(4).setIcon(R.mipmap.fb_groups);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_favorites, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.aboutMeActivity) {
            Intent intent = new Intent(this, AboutMeActivity.class);
            startActivity(intent);
            // Handle the camera action
        } else if (id == R.id.homeActivity) {
            Intent intent = new Intent(this, HomeScreen.class);
            startActivity(intent);

        } else if (id == R.id.favActivity) {
            Intent intent = new Intent(this, FavoritesActivity.class);
            startActivity(intent);
//           EditText searchEditText = (EditText) findViewById(R.id.searchBar);
//           String searchQuery = searchEditText.getText().toString();
//           if(searchQuery!=null && searchQuery.trim().length()!=0){
//               intent.putExtra("searchQuery", searchQuery);
//               startActivity(intent);
//           }

        } /*else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public static SharedPreferences getSharedPreferences (Context context) {
            return context.getSharedPreferences("Favorites", MODE_PRIVATE);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {



            favItemsUsers = new ArrayList<>();
            favItemsPages = new ArrayList<>();
            favItemsPlaces = new ArrayList<>();
            favItemsGroups = new ArrayList<>();
            favItemsEvents = new ArrayList<>();
            View rootView = inflater.inflate(R.layout.fragment_results, container, false);
            Bundle args = getArguments();
            Gson gson = new Gson();
            SharedPreferences sharedFavs = getSharedPreferences(this.getContext());
            Map<String,?> keys = sharedFavs.getAll();

            for(Map.Entry<String,?> sharedPref : keys.entrySet()) {

                String rowValue = sharedFavs.getString(sharedPref.getKey(), sharedPref.getValue().toString());
                ResultItem favObj = gson.fromJson(rowValue, ResultItem.class);

                if (favObj.getProfiletype().equals("user")) {
                    favItemsUsers.add(favObj);
                    favObj.setProfilefav(true);
                }
                else if (favObj.getProfiletype().equals("page")) {
                    favItemsPages.add(favObj);
                    favObj.setProfilefav(true);
                }
                else if (favObj.getProfiletype().equals("group")) {
                    favItemsGroups.add(favObj);
                    favObj.setProfilefav(true);
                }
                else if (favObj.getProfiletype().equals("event")) {
                    favItemsEvents.add(favObj);
                    favObj.setProfilefav(true);
                }
                else if (favObj.getProfiletype().equals("place")) {
                    favItemsPlaces.add(favObj);
                    favObj.setProfilefav(true);
                }

            }

            ListView resultView = (ListView) rootView.findViewById(R.id.profilesListView);
            int selectedTab = args.getInt(ARG_SECTION_NUMBER) - 1;

            ListAdapter resultsAdapter = null;

            if (selectedTab == 0) {
                rootView.setTag("user");
               resultsAdapter = new customAdapter(getContext(), R.layout.results_row, favItemsUsers);
            } else if (selectedTab == 1) {
                rootView.setTag("page");
                resultsAdapter = new customAdapter(getContext(), R.layout.results_row, favItemsPages);
            } else if (selectedTab == 2) {
                rootView.setTag("event");
                resultsAdapter = new customAdapter(getContext(), R.layout.results_row, favItemsEvents);
            } else if (selectedTab == 3) {
                rootView.setTag("place");
                resultsAdapter = new customAdapter(getContext(), R.layout.results_row, favItemsPlaces);
            } else if (selectedTab == 4) {
                rootView.setTag("group");
                resultsAdapter = new customAdapter(getContext(), R.layout.results_row, favItemsGroups);
            }

            resultView.setAdapter(resultsAdapter);
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Users";
                case 1:
                    return "Pages";
                case 2:
                    return "Events";
                case 3:
                    return "Places";
                case 4:
                    return "Groups";
            }
            return null;
        }
    }
}
