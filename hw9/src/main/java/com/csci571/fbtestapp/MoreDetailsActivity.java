package com.csci571.fbtestapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.facebook.share.widget.ShareDialog;
import com.facebook.share.ShareApi;
import com.facebook.*;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.R.attr.data;
import static android.R.attr.id;
import static android.R.attr.type;
import static android.R.attr.visible;
import static com.csci571.fbtestapp.R.id.moreDetailsExpdListView;
import static com.csci571.fbtestapp.R.id.share;
import static com.csci571.fbtestapp.ResultsActivity.readTimeoutVal;
import static com.csci571.fbtestapp.ResultsActivity.reqTypeMethod;
import static com.csci571.fbtestapp.ResultsActivity.timeOutVal;

public class MoreDetailsActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    ShareDialog shareDialog;
    CallbackManager callbackManager;
    private Menu menu;
    private static String testDetaN;
    private static String testDetaI;
    private static String testDetaU;
    private static String testDetaT;
    private static String deleteFavorite=null;
    public static int timeOutVal = 15000;
    public static String reqTypeMethod = "GET";
    public static int readTimeoutVal = 15000;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        setupTabIcons();



        Intent intent = getIntent();
        testDetaI = intent.getStringExtra("profileI");
        testDetaN = intent.getStringExtra("profileN");
        testDetaU = intent.getStringExtra("profileU");
        testDetaT = intent.getStringExtra("profileT");
        callbackManager = CallbackManager.Factory.create();
        //shareDialog = new ShareDialog(this);
        //shareDialog.registerCallback(callbackManager, shareCallBack);

        FacebookSdk.sdkInitialize(getApplicationContext());

    }

    public void sharetoFB() {

        Bitmap image = BitmapFactory.decodeResource(getResources(),R.mipmap.fb_favorites_on);
        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle(testDetaN)
                .setContentDescription("FB SEARCH FROM USC CSCI571")
                .setImageUrl(Uri.parse(testDetaU))
                .setContentUrl(Uri.parse("https://developers.facebook.com/docs"))
                .build();
        ShareDialog shareDialog1 = new ShareDialog(this);
        shareDialog1.registerCallback(callbackManager,shareCallBack);
        shareDialog1.show(linkContent);
    }



   /* public ShareDialog callFBShare(com.facebook.share.model.ShareLinkContent linkContent) {
return ShareDialog.show(linkContent,null);
        //return context.getSharedPreferences("Favorites", MODE_PRIVATE);
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public FacebookCallback<Sharer.Result> shareCallBack = new FacebookCallback<Sharer.Result>() {

        @Override
        public void onSuccess(Sharer.Result result) {
            Toast toast = Toast.makeText(getApplicationContext(), "Posted successfully", Toast.LENGTH_SHORT);
            toast.show();
        }

        @Override
        public void onCancel() {
            Toast toast = Toast.makeText(getApplicationContext(), "Not Posted", Toast.LENGTH_SHORT);
            toast.show();
        }

        @Override
        public void onError(FacebookException error) {
            Toast toast = Toast.makeText(getApplicationContext(), "Not Posted", Toast.LENGTH_SHORT);
            toast.show();
        }
    };




    private void refreshMenu() {
        if (getFavorites(testDetaI)) {
            //Log.d("Menu Item id", testDetaI + "*" + R.id.remove_from_favorites);
            enableFav(R.id.remove_from_favorites);
            enableFav(R.id.share);
            disableFav(R.id.add_to_favorites);
        } else {
            enableFav(R.id.add_to_favorites);
            enableFav(R.id.share);
            disableFav(R.id.remove_from_favorites);
        }
    }




    private void setupTabIcons() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setIcon(R.mipmap.fb_albums);
        tabLayout.getTabAt(1).setIcon(R.mipmap.fb_posts);

    }

    public boolean getFavorites(String profileid){
        SharedPreferences mPrefs = getSharedPreferences("Favorites", MODE_PRIVATE);
        if(!mPrefs.contains(profileid)){
            return false;
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_more_details, menu);
        this.menu = menu;
        refreshMenu();
        return true;
    }

    private void disableFav(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    private void enableFav(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.add_to_favorites) {
            Gson gsonObj = new Gson();
            deleteFavorite="false";
            SharedPreferences settings = getSharedPreferences("Favorites", 0);
            SharedPreferences.Editor editor = settings.edit();
            ProfileItem newItem = new ProfileItem(testDetaI,testDetaN,testDetaU,testDetaT);
            editor.putString(testDetaI, gsonObj.toJson(newItem));
            editor.commit();
            refreshMenu();
            Toast toast = Toast.makeText(getApplicationContext(), "Added to Favorites", Toast.LENGTH_SHORT);
            toast.show();

        }

        if (id == R.id.remove_from_favorites) {
            deleteFavorite="true";
            SharedPreferences settings = getSharedPreferences("Favorites", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.remove(testDetaI);
            editor.commit();
            refreshMenu();
            Toast toast = Toast.makeText(getApplicationContext(), "Removed from Favorites", Toast.LENGTH_SHORT);
            toast.show();
        }



        if (id == R.id.share) {
            sharetoFB();
            //return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putString("nDeta", testDetaN);
            args.putString("iDeta", testDetaI);
            args.putString("uDeta", testDetaU);
            args.putString("tDeta", testDetaT);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            ExpandableListView expandableListView = null;
            ExpandableListAdapter expandableListAdapter;
            Bundle args = getArguments();
            String empty_albums = null, empty_posts = null;
            String did = args.getString("iDeta");
            ListView postsListview = null;
            String dname = args.getString("nDeta");
            CustomPostsAdapter postsAdapter;
            String durl = args.getString("uDeta");
            final List<String> expandableListTitle = new ArrayList<>();
            String dtype = args.getString("tDeta");
            PostData newPostmsg;
            String postmsgData;
            String postCreatedTime;
            JSONObject photonName;
            String albumName;

            int currentView = args.getInt(ARG_SECTION_NUMBER) - 1;
            final List<String> postsData = new ArrayList<>();


            final HashMap<String, List<String>> expandableListDetail = new HashMap<>();
            String callURL = "http://fb-app-test.appspot.com/main.php?idValue=" + did + "&type=" + dtype;
            View detailsResultView = inflater.inflate(R.layout.fragment_more_details, container, false);
            JSONObject mainDetailsResult = null;
            List<PostData> postData = new ArrayList<>();

            if (currentView == 0) {
                expandableListView = (ExpandableListView) detailsResultView.findViewById(moreDetailsExpdListView);
            } else {
             postsListview = (ListView) detailsResultView.findViewById(R.id.postsListview);
            }

            String detailsResult = null;
            try {
                HttpGetRequest getRequest = new HttpGetRequest();
                detailsResult = getRequest.execute(callURL).get();
                mainDetailsResult = new JSONObject(detailsResult);
            } catch (Exception e) {
            }

            try {
                if (detailsResult != null) {
                    if (!mainDetailsResult.has("albums")) {
                        TextView zeroAlbums = (TextView) detailsResultView.findViewById(R.id.zeroAlbums);
                        zeroAlbums.setVisibility(zeroAlbums.VISIBLE);
                        TextView zeroPosts = (TextView) detailsResultView.findViewById(R.id.zeroPosts);
                        zeroPosts.setVisibility(zeroPosts.INVISIBLE);

                    } else {
                        JSONObject detailsAlbums = mainDetailsResult.getJSONObject("albums");
                        JSONArray detailsalb1 = detailsAlbums.getJSONArray("data");
                        for (int len = 0; len < detailsalb1.length(); len++) {

                            photonName = detailsalb1.getJSONObject(len);
                            if (photonName.has("name")){
                            if (photonName.has("photos")){
                                List<String> photoList = new ArrayList<>();
                                albumName = photonName.getString("name");
                                expandableListTitle.add(albumName);
                                JSONObject photosData = photonName.getJSONObject("photos");
                                JSONArray photoURL = photosData.getJSONArray("data");

                                for (int pic = 0; pic < photoURL.length(); pic++) {
                                    JSONObject highReID = photoURL.getJSONObject(pic);
                                    String pfID = highReID.getString("id");
                                    photoList.add("https://graph.facebook.com/v2.8/" + pfID + "/picture?type(large)&access_token=EAASSWS9fsxYBAFZCRCaSxxnFgN5mnyVaZAXM6B8YTthKjdVD3RP9kMyXcZCD64s9zUP4kFNfZCbs5HgzqLWTgs2395dtx532eROgY65hpce6s5cdcQP5QIPbRUjw3S9i8qf6B2gxipP8ZAT1z0aL4");
                                }
                                expandableListDetail.put(albumName, photoList);
                            }
                        }
                        }


                    }

                    if (currentView != 0) {
                    }
                    else
                    {
                        expandableListAdapter = new CustomExpandableListAdapter(getContext(), expandableListTitle, expandableListDetail, empty_albums);
                        expandableListView.setAdapter(expandableListAdapter);
                    }
                } else {

                }
            } catch (JSONException e) {


            } catch (Exception e) {


            }
         try {
            if (detailsResult!=null) {
                if(!mainDetailsResult.has("posts")) {

                }
                else{

                    JSONObject detailsPosts = mainDetailsResult.getJSONObject("posts");
                    JSONArray detaPostData = detailsPosts.getJSONArray("data");
                    for (int k = 0; k < detaPostData.length(); k++) {
                        JSONObject msgPostObject = detaPostData.getJSONObject(k);
                        if (msgPostObject.has("message")){
                                if(msgPostObject.has("created_time")) {
                            postmsgData = msgPostObject.getString("message");
                            postCreatedTime = msgPostObject.getString("created_time");
                            newPostmsg = new PostData(postmsgData, postCreatedTime);
                            postData.add(newPostmsg);
                        }}
                    }
                }

                if(currentView==0){

                }
                else {

                    if(!mainDetailsResult.has("posts")) {
                        TextView zeroAlbums = (TextView) detailsResultView.findViewById(R.id.zeroAlbums);
                        zeroAlbums.setVisibility(zeroAlbums.INVISIBLE);

                        TextView zeroPosts = (TextView) detailsResultView.findViewById(R.id.zeroPosts);
                        zeroPosts.setVisibility(zeroPosts.VISIBLE);
                    }
                    postsAdapter = new CustomPostsAdapter(getContext(), R.layout.detailspostrows, postData, testDetaN, testDetaU, empty_posts);
                    postsListview.setAdapter(postsAdapter);
                }
            } else {

            }
        } catch (JSONException e) {

            e.printStackTrace();
        } catch (Exception e) {

            e.printStackTrace();
        }

            return detailsResultView;
        }

    }




    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        // public SectionsPagerAdapter(FragmentManager fm) {
        //   super(fm);
        //}

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Albums";
                case 1:
                    return "Posts";
            }
            return null;
        }
    }
}

